package utils.positions

import play.api.libs.json.Format
import play.api.libs.json._ // JSON library
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._

/**
 * Simple location
 *
 * @param lat
 * @param lng
 */
case class Position(
  lat: Double,
  lng: Double) {

  /**
   * Use haversine distance to check if this is inside other
   * @param other location to check
   * @return the haversine distance between this and other position
   */
  def distance(other: Position): Double = Haversine.haversine(lat1 = lat, lon1 = lng, lat2 = other.lat, lon2 = other.lng)
}

/**
 * Companion object
 */
object Position {
  implicit val locationFormat: Format[Position] = (
    (JsPath \ "lat").format[Double](min(-90.0) keepAnd max(90.0)) ~
    (JsPath \ "lng").format[Double](min(-180.0) keepAnd max(180.0)))(Position.apply, unlift(Position.unapply))
}
