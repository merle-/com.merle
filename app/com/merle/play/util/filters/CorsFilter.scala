package utils.filters

import play.api.mvc._
import play.api.http.HeaderNames._
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext

/**
 * This Filter add Cors Header to all response
 */
object CorsFilter extends Filter {
  def apply(nextFilter: (RequestHeader) => Future[Result])(requestHeader: RequestHeader): Future[Result] = {

    val origin = requestHeader.headers.get(ORIGIN).getOrElse("*")
    
    def cors(result: Result): Result = {
      result.withHeaders(ACCESS_CONTROL_ALLOW_ORIGIN -> origin,
        ACCESS_CONTROL_ALLOW_METHODS -> "POST, GET, OPTIONS, PUT, DELETE",
        ACCESS_CONTROL_ALLOW_HEADERS -> "Origin, Content-Type, X-Requested-With, Accept, X-Csrftoken, Authorization",
        ACCESS_CONTROL_ALLOW_CREDENTIALS -> "true")
    }

    nextFilter(requestHeader) map { result =>
      cors(result)
    }
  }

}