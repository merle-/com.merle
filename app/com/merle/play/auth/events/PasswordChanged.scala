package com.merle.play.auth.events

import play.api.mvc._
import play.api.i18n.Lang
import com.mohiva.play.silhouette.api._

/**
 * An event which will be published after an identity change password.
 *
 * @param identity who changed password.
 * @param request The request header for the associated request.
 * @param lang The language associated for the current request.
 * @param I The type of the identity.
 */
case class PasswordChanged[I <: Identity](identity: I, request: RequestHeader, lang: Lang) extends SilhouetteEvent