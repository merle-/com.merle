package com.merle.play.auth.controllers.rest

import scala.concurrent.{ Future, ExecutionContext }
import play.api.Play
import play.api.Play.current
import play.api.mvc.Action
import play.api.libs.json._
import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.api.services._
import com.mohiva.play.silhouette.impl.providers._
import com.mohiva.play.silhouette.api.exceptions.AuthenticationException
import com.mohiva.play.silhouette.impl.authenticators.{ JWTAuthenticator, JWTAuthenticatorService }

import com.merle.play.util.responses.rest._
import com.merle.play.auth.models.{ Token, SocialAuth }
import com.merle.play.auth.users.GenericUser
import com.merle.play.auth.services.UserService

/**
 * This controller manage authentication of an user by social service (like facebook and other)
 */
trait SocialAuthController[I <: GenericUser, A <: JWTAuthenticator] extends Silhouette[I, A] {

  val userService: UserService[I]
  val authInfoService: AuthInfoService
  val authenticatorHeaderName = Play.configuration.getString("silhouette.authenticator.headerName").getOrElse { "X-Auth-Token" }
  implicit val ec: ExecutionContext = play.api.libs.concurrent.Execution.Implicits.defaultContext

  /**
   * Util method to use for retrieve information from authInfo
   *
   * @param provider where retrieve information
   * @param socialAuth object where get auth information
   * @return a pair with CommonSocialProfile and AuthInfo
   */
  protected def profileAndAuthInfo(provider: String, socialAuth: SocialAuth) = {
    env.providers.get(provider) match {
      case Some(p: OAuth1Provider with CommonSocialProfileBuilder) => //for OAuth1 provider type
        val authInfo = OAuth1Info(token = socialAuth.token, socialAuth.secret.get)
        p.retrieveProfile(authInfo).map(profile => (profile, authInfo))
      case Some(p: OAuth2Provider with CommonSocialProfileBuilder) => //for OAuth2 provider type
        val authInfo = OAuth2Info(accessToken = socialAuth.token, expiresIn = socialAuth.expiresIn)
        p.retrieveProfile(authInfo).map(profile => (profile, authInfo))
      case _ => Future.successful(new AuthenticationException(s"Cannot retrive information with unexpected social provider $provider"))
    }
  }

  /**
   * Authenticates a user against a social provider.
   *
   * receive json like this:
   * {
   * 	  "accessToken": "...",
   *  	"expiresIn": 0000, //optional
   *  	"secret": "..."  //this is for OAuth1, for OAuth2 isn't request
   * }
   * or
   * {
   *    "code": "...",
   *    ...
   * }
   * @param provider The ID of the provider to authenticate against.
   * @return The result to display.
   */
  def authenticate(provider: String) = Action.async(parse.json) { implicit request =>
    request.body.validate[SocialAuth] match {
      case JsSuccess(socialAuth, _) =>
        (profileAndAuthInfo(provider, socialAuth).flatMap {
          case (profile: CommonSocialProfile, authInfo: AuthInfo) =>
            (for {
              user <- userService.save(profile)
              authInfo <- authInfoService.save(profile.loginInfo, authInfo)
              authenticator <- env.authenticatorService.create(profile.loginInfo)
            } yield {
              env.eventBus.publish(LoginEvent(user, request, request2lang))
              /** init token and send it to body */
              env.authenticatorService.init(authenticator, Future.successful(Ok)).map{r =>
                val token = r.header.headers.get(authenticatorHeaderName).get
                Ok(Json.toJson(Token(token = token, expiresOn = authenticator.expirationDate)))
              }
            }).flatMap(r => r)
        }).recoverWith(exceptionHandler)
      case JsError(e) => Future.successful(BadRequest(Json.obj("message" -> JsError.toFlatJson(e))))
    }
  }

  /**
   * Link social with a existing user.
   *
   * receive json like this:
   * {
   * 	"accessToken": "...",
   *  	"expiresIn": 0000, //optional
   *  	"secret": "..."  //this is for OAuth1, for OAuth2 isn't request
   * }
   *
   * @param provider The ID of the provider to authenticate against.
   * @return The result to display.
   */
  def link(provider: String) = SecuredAction.async(parse.json) { implicit request =>
    request.body.validate[SocialAuth] match {
      case JsSuccess(socialAuth, _) =>
        (profileAndAuthInfo(provider, socialAuth).flatMap {
          case (profile: CommonSocialProfile, authInfo: AuthInfo) =>
            (for {
              user <- userService.link(request.identity, profile)
              authInfo <- authInfoService.save(profile.loginInfo, authInfo)
              authenticator <- env.authenticatorService.create(profile.loginInfo)
            } yield {
              env.eventBus.publish(LoginEvent(user, request, request2lang))
              val response = Ok(Json.toJson(Token(token = authenticator.id, expiresOn = authenticator.expirationDate)))
              env.authenticatorService.init(authenticator, Future.successful(response))
            }).flatMap(r => r)
        }).recoverWith(exceptionHandler)
      case JsError(e) => Future.successful(BadRequest(Json.obj("message" -> JsError.toFlatJson(e))))
    }
  }

}