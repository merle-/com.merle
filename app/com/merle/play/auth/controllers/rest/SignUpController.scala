package com.merle.play.auth.controllers.rest

import scala.concurrent.{ Future, ExecutionContext }
import play.api.Play
import play.api.Play.current
import play.api.mvc.Action
import play.api.libs.json._
import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.api.util._
import com.mohiva.play.silhouette.api.services._
import com.mohiva.play.silhouette.impl.providers._
import com.mohiva.play.silhouette.impl.authenticators.{JWTAuthenticator, JWTAuthenticatorService}

import com.merle.play.util.responses.rest._
import com.merle.play.auth.models.{ Token, SignUp }
import com.merle.play.auth.users.GenericUser
import com.merle.play.auth.services.UserService

/**
 * This controller manage registration of an user
 */
trait SignUpController[I <: GenericUser, A <: JWTAuthenticator] extends Silhouette[I, A] {
  val userService: UserService[I]
  val authInfoService: AuthInfoService
  val avatarService: AvatarService
  val passwordHasher: PasswordHasher
  val authenticatorHeaderName = Play.configuration.getString("silhouette.authenticator.headerName").getOrElse { "X-Auth-Token" }
  implicit val ec: ExecutionContext = play.api.libs.concurrent.Execution.Implicits.defaultContext

  /**
   * The formats for read json represent SignUp
   */
  implicit val signUpFormat = Json.format[SignUp]
  
  /**
   * Registers a new user.
   *
   * receive call with json like this:
   * 	{
   * 		"password": "",
   * 		"identifier": "",
   *  		"firstName": "",
   *    	"lastName": "",
   *     	"fullName": ""
   * 	}
   *
   * @return The result to display.
   */
  def signUp = Action.async(parse.json) { implicit request =>
    request.body.validate[SignUp] match {
      case JsSuccess(signUp, _) =>
        val loginInfo = LoginInfo(CredentialsProvider.ID, signUp.identifier)
        (userService.retrieve(loginInfo).map {
          case None => /* user not already exists */
            val authInfo = passwordHasher.hash(signUp.password)
            val user = userService.create(loginInfo, signUp)
            (for {
              avatar <- avatarService.retrieveURL(signUp.identifier)
              createdUser <- userService.create(loginInfo, signUp, avatar, json = request.body)
              user <- userService.save(createdUser)
              authInfo <- authInfoService.save(loginInfo, authInfo)
              authenticator <- env.authenticatorService.create(user.loginInfo)
            } yield {
              env.eventBus.publish(SignUpEvent(user, request, request2lang))
              env.eventBus.publish(LoginEvent(user, request, request2lang))
              /** init token and send it to body */
              env.authenticatorService.init(authenticator, Future.successful(Ok)).map{r =>
                val token = r.header.headers.get(authenticatorHeaderName).get
                Ok(Json.toJson(Token(token = token, expiresOn = authenticator.expirationDate)))
              }
            }).flatMap { r => r }
          case Some(u) => /* user already exists! */
            Future.successful(Conflict(Json.toJson(Bad(message = "user already exists"))))
        }).flatMap { r => r }
      case JsError(e) => Future.successful(BadRequest(Json.toJson(Bad(message = JsError.toFlatJson(e)))))
    }
  }

  /**
   * Handles the Sign Out action.
   *
   * @return The result to display.
   */
  def signOut = UserAwareAction.async { implicit request =>
    request.identity match {
      case Some(identity) =>
        env.authenticatorService.create(identity.loginInfo).flatMap { authenticator =>
          env.eventBus.publish(LogoutEvent(identity, request, request2lang))
          val response = Ok(Json.toJson(Good(message = "logout completed")))
          authenticator.discard(Future.successful(response))
        }
      case None => Future.successful(Ok(Json.toJson(Good(message = "logout completed"))))
    }
  }

}