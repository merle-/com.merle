package com.merle.play.auth.controllers.rest

import java.util.UUID
import scala.concurrent.{ Future, ExecutionContext }
import play.api.mvc.Action
import play.api.libs.json._
import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.api.util._
import com.mohiva.play.silhouette.api.services._
import com.mohiva.play.silhouette.impl.providers._
import com.mohiva.play.silhouette.impl.authenticators.JWTAuthenticator

import com.merle.play.util.responses.rest._
import com.merle.play.auth.services.MailService
import com.merle.play.auth.users.GenericUser
import com.merle.play.auth.services.UserService

/**
 * 
 */
trait ResetPasswordController[I <: GenericUser, A <: JWTAuthenticator] extends Silhouette[I, A] {

  val userService: UserService[I]
  val authInfoService: AuthInfoService
  val avatarService: AvatarService
  val cacheLayer: CacheLayer
  val passwordHasher: PasswordHasher
  val mailService: MailService
  implicit val ec: ExecutionContext = play.api.libs.concurrent.Execution.Implicits.defaultContext

  val cacheExpiration = 30 * 60 // 30 minutes

  /**
   * Send email for reset password.
   *
   * receive call with json like this:
   * 	{
   * 		"email": ""
   * 	}
   *
   * @return The result to display.
   */
  def resetPasswordEmail = Action.async(parse.json) { implicit request =>
    (request.body \ "email").validate[String] match {
      case JsSuccess(email, _) => userService.retrieve(LoginInfo(CredentialsProvider.ID, email)).flatMap {
        case Some(user) =>
          val cacheID = UUID.randomUUID().toString
          cacheLayer.save(cacheID, user, cacheExpiration)
          play.Logger.debug(cacheID)
          /**TODO: Send email*/
          mailService.sendPasswordResetEmail(user, cacheID)
          Future.successful(Ok(Json.toJson(Good(message = "check your mail man!"))))
        case None => Future.successful(NotFound(Json.toJson(Bad(message = "I cant found you!"))))
      }
      case e: JsError => Future.successful(BadRequest(Json.toJson(Bad(message = JsError.toFlatJson(e)))))
    }
  }

  /**
   * Reset password of an user.
   *
   * receive call with json like this:
   * 	{
   * 		"password": "" //the new password to set
   * 	}
   *
   * @param id of request for reset password
   * @return The result to display.
   */
  def resetPassword(id: String) = Action.async(parse.json) { implicit request =>
    (request.body \ "password").validate[String] match {
      case JsSuccess(newPassword, _) => cacheLayer.find[GenericUser](id).flatMap {
        case Some(user) if (user.loginInfo.providerID == CredentialsProvider.ID) =>
          val authInfo = passwordHasher.hash(newPassword)
          authInfoService.save(user.loginInfo, authInfo).map { authInfo =>
            env.eventBus.publish(LoginEvent(user, request, request2lang))
            Ok(Json.toJson(Good(message = "password changed!")))
          }
        case Some(user) =>
          /** have logged id only with external socials */
          /** TODO: Register new user with email and password provided */
          Future.successful(Ok(Json.toJson(Bad(message = "Have logged id only with external socials"))))
        case None =>
          /** incorrect id */
          Future.successful(NotFound(Json.toJson(Bad(message = "I cant found you!"))))
      }
      case e: JsError => Future.successful(BadRequest(Json.toJson(Bad(message = JsError.toFlatJson(e)))))
    }
  }
}