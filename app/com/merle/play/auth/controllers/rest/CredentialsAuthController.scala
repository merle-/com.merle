package com.merle.play.auth.controllers.rest

import scala.concurrent.{ Future, ExecutionContext }
import play.api.Play
import play.api.Play.current
import play.api.mvc.Action
import play.api.libs.json._
import com.mohiva.play.silhouette.api.{ Silhouette, LoginEvent }
import com.mohiva.play.silhouette.api.util.Credentials
import com.mohiva.play.silhouette.impl.providers.CredentialsProvider
import com.mohiva.play.silhouette.api.exceptions.AuthenticationException
import com.mohiva.play.silhouette.api.services.AuthInfoService
import com.mohiva.play.silhouette.impl.authenticators.{ JWTAuthenticator, JWTAuthenticatorService }

import com.merle.play.auth.models.Token
import com.merle.play.auth.users.GenericUser
import com.merle.play.auth.services.UserService

/**
 * This controller manage authentication of an user by identifier and password
 */
trait CredentialsAuthController[I <: GenericUser, A <: JWTAuthenticator] extends Silhouette[I, A] {

  val userService: UserService[I]
  val authInfoService: AuthInfoService
  val authenticatorHeaderName = Play.configuration.getString("silhouette.authenticator.headerName").getOrElse { "X-Auth-Token" }
  implicit val ec: ExecutionContext = play.api.libs.concurrent.Execution.Implicits.defaultContext

  /**
   *
   */
  implicit val restCredentialFormat = com.merle.play.auth.formatters.json.CredentialFormat.restFormat

  /**
   * Authenticates a user against the credentials provider.
   *
   * receive json like this:
   * {
   * 	"identifier": "...",
   *  	"password": "..."
   * }
   *
   * @return The result to display.
   */
  def authenticate = Action.async(parse.json) { implicit request =>
    request.body.validate[Credentials] match {
      case JsSuccess(credentials, _) =>
        (env.providers.get(CredentialsProvider.ID) match {
          case Some(p: CredentialsProvider) => p.authenticate(credentials)
          case _ => Future.failed(new AuthenticationException(s"Cannot find credentials provider"))
        }).flatMap { loginInfo =>
          userService.retrieve(loginInfo).flatMap {
            case Some(user) => env.authenticatorService.create(user.loginInfo).flatMap { authenticator =>
              env.eventBus.publish(LoginEvent(user, request, request2lang))
              /** init token and send it to body */
              env.authenticatorService.init(authenticator, Future.successful(Ok)).map { r =>
                val token = r.header.headers.get(authenticatorHeaderName).get
                Ok(Json.toJson(Token(token = token, expiresOn = authenticator.expirationDate)))
              }
            }
            case None =>
              Future.failed(new AuthenticationException("Couldn't find user"))
          }
        }.recoverWith(exceptionHandler)
      case JsError(e) => Future.successful(BadRequest(Json.obj("message" -> JsError.toFlatJson(e))))
    }
  }

}