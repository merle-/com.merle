package com.merle.play.auth.users

import com.mohiva.play.silhouette.api.{ Identity, LoginInfo }
import com.merle.play.auth.authorizations.Role

/**
 * Basic information of an user
 *
 * @param firstName
 * @param lastName
 * @param fullName
 * @param gender
 */
trait GenericInfo {
  def firstName: Option[String]
  def lastName: Option[String]
  def fullName: Option[String]
  def gender: Option[String]
}

/**
 * This trait represent a generic user of this platform
 *
 * @param loginInfo unique information of user
 * @param socials the social connected to user
 * @param email
 * @param username
 * @param avatarUrl
 * @param info generic information of user
 * @param roles
 */
trait GenericUser extends Identity {
  def loginInfo: LoginInfo
  def socials: Option[Seq[LoginInfo]]
  def email: Option[String]
  def username: Option[String]
  def avatarUrl: Option[String]
  def info: GenericInfo
  def roles: Set[Role]
}

