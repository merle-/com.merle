package com.merle.play.auth.authorizations

/**
 * Represent the role
 * 
 * @param name string representation of role
 */
trait Role {
	def name: String
}