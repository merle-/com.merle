package com.merle.play.auth.services

import play.api.libs.concurrent.Akka
import play.api.Play.current
import play.api.mvc.RequestHeader
import play.api.i18n.Lang
import play.twirl.api.{ Txt, Html }
import com.typesafe.plugin._
import scala.concurrent.duration._
import play.api.libs.concurrent.Execution.Implicits._
import akka.actor._
import com.mohiva.play.silhouette.api._
import com.merle.play.auth.events._
import com.merle.play.auth.users.GenericUser

/**
 *
 */
trait MailTemplateService {
  /**
   *
   */
  def welcomeEmailTemplate(user: GenericUser)(implicit request: RequestHeader, lang: Lang): play.twirl.api.HtmlFormat.Appendable
  /**
   *
   */
  def passwordResetEmailTemplate(user: GenericUser, token: String)(implicit request: RequestHeader, lang: Lang): play.twirl.api.HtmlFormat.Appendable
  /**
   *
   */
  def passwordChangedNoticeTemplate(user: GenericUser)(implicit request: RequestHeader, lang: Lang): play.twirl.api.HtmlFormat.Appendable

}

/**
 *
 */
trait MailService {
  /**
   *
   */
  val mailTemplateService: MailTemplateService

  /**
   *
   */
  val fromAddress = current.configuration.getString("smtp.from").get

  /**
   * Define what do on listener event
   */
  val listener = Akka.system.actorOf(Props(new Actor {
    def receive = {
      case e @ SignUpEvent(identity: GenericUser, request, lang) =>
        play.Logger.info(s"Send Email SignUp to: ${identity}")
        sendWelcomeEmail(identity)(request, lang)
      case e @ PasswordChanged(identity: GenericUser, request, lang) =>
        play.Logger.info(s"Send Email Password Changed to: ${identity}")
        sendPasswordChangedNotice(identity)(request, lang)
      case e @ LoginEvent(identity, request, lang) =>
        play.Logger.info(s"User logged in: ${e}")
      case e @ LogoutEvent(identity, request, lang) =>
        play.Logger.info(s"User logged out: ${e}")
    }
  }))

  /**
   * Set up listener
   */
  def startListener = {
    val eventBus = EventBus()
    eventBus.subscribe(listener, classOf[LoginEvent[GenericUser]])
    eventBus.subscribe(listener, classOf[LogoutEvent[GenericUser]])
    eventBus.subscribe(listener, classOf[SignUpEvent[GenericUser]])
    eventBus.subscribe(listener, classOf[SignUpEvent[GenericUser]])
  }

  def sendWelcomeEmail[I <: GenericUser](user: I)(implicit request: RequestHeader, lang: Lang) = {
    val txtAndHtml = (None, Some(mailTemplateService.welcomeEmailTemplate(user)(request, lang)))
    sendEmail("Welcome!!!!", user.email.get, txtAndHtml)
  }

  def sendPasswordResetEmail[I <: GenericUser](user: I, token: String)(implicit request: RequestHeader, lang: Lang) = {
    val txtAndHtml = (None, Some(mailTemplateService.passwordResetEmailTemplate(user, token)(request, lang)))
    sendEmail("Reset password", user.email.get, txtAndHtml)
  }

  def sendPasswordChangedNotice[I <: GenericUser](user: I)(implicit request: RequestHeader, lang: Lang) = {
    val txtAndHtml = (None, Some(mailTemplateService.passwordChangedNoticeTemplate(user)(request, lang)))
    sendEmail("Password changed successful", user.email.get, txtAndHtml)
  }

  /**
   *
   */
  def sendEmail(subject: String, recipient: String, body: (Option[Txt], Option[Html])) = {

    play.Logger.debug(s"[securesocial] sending email to $recipient")
    play.Logger.debug(s"[securesocial] mail = [$body]")

    Akka.system.scheduler.scheduleOnce(1 seconds) {
      val mail = use[MailerPlugin].email
      mail.setSubject(subject)
      mail.setRecipient(recipient)
      mail.setFrom(fromAddress)
      // the mailer plugin handles null / empty string gracefully
      mail.send(body._1.map(_.body).getOrElse(""), body._2.map(_.body).getOrElse(""))
    }
  }

}

class DefaultMailTemplateService extends MailTemplateService {
  /**
   *
   */
  def welcomeEmailTemplate(user: GenericUser)(implicit request: RequestHeader, lang: Lang) = {
    com.merle.play.auth.views.html.mails.welcomeEmail(user)(request, lang)
  }
  /**
   *
   */
  def passwordResetEmailTemplate(user: GenericUser, token: String)(implicit request: RequestHeader, lang: Lang) = {
    com.merle.play.auth.views.html.mails.passwordResetEmail(user, token)(request, lang)
  }
  /**
   *
   */
  def passwordChangedNoticeTemplate(user: GenericUser)(implicit request: RequestHeader, lang: Lang) = {
    com.merle.play.auth.views.html.mails.passwordChangedNotice(user)(request, lang)
  }
}

class DefaultMailService extends MailService {
  
  lazy val mailTemplateService = new DefaultMailTemplateService  

}