package com.merle.play.storage.mongo

import com.merle.play.storage.Entity

trait Identifiable extends Entity {
  
  val iod: String = id.iod
  
  def id: StorageId

  /**
   * Return created date
   */
  def created: Long = Integer.parseInt(id.iod.slice(0, 8), 16).toLong * 1000
}
