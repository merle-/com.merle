package com.merle.play.storage.mongo

import com.merle.play.storage.Entity
import reactivemongo.bson.BSONObjectID

/**
 * Represent a ($)oid of mongoDB, iod can be called ($)iod like in mongoDB, but i don't like so much
 */
class StorageId(val iod: String) extends Entity {
  
  override def equals(other: Any) = other match {
    case id: Entity => iod == id.iod
    case _ => super.equals(other)
  }
  
}

/**
 * Companion object for class StorageId
 */
object StorageId {

  /** format for mongoDB */
  implicit val storageFormat = storage.mongo.formatters.StorageIdFormats.storageFormat

  /**
   *
   */
  def generate = new StorageId(BSONObjectID.generate.stringify)

  /**
   *
   */
  def apply(oid: String): StorageId = {
    new StorageId(oid)
  }

  /**
   *
   */
  def unapply(storageId: StorageId): Option[String] = {
    Some(storageId.iod)
  }
}