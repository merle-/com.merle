package com.merle.play.storage.mongo

import scala.concurrent.{ ExecutionContext, Future }
import reactivemongo.api._
import reactivemongo.api.collections._
import reactivemongo.core.commands._
import reactivemongo.bson.BSONObjectID
import play.modules.reactivemongo._
import play.modules.reactivemongo.json.collection.JSONCollection
import play.modules.reactivemongo.ReactiveMongoPlugin
import play.api.Play.current
import play.api.libs.iteratee.Enumerator
import play.api.libs.json._
import play.api.libs.json.Json.toJsFieldJsValueWrapper
import com.merle.play.storage.Entity

/**
 * Represent object that store objects of type T
 *
 */
trait DataAccess[T <: Entity] {

  /**
   *
   */
  def collectionName: String

  /**
   *
   */
  implicit val ec: ExecutionContext = ExecutionContext.Implicits.global

  /** Switch to implicit lazy val on production */
  implicit def db = ReactiveMongoPlugin.db

  /** Switch to implicit val on production */
  def collection: JSONCollection = db.collection[JSONCollection](collectionName)

  /** Format for storage data */
  implicit val storageFormat: Format[T]

  //  /** Generates a new ID and adds it to your JSON using Json extended notation for BSON */
  //  val generateID = (__ \ '_id \ '$oid).json.put(JsString(BSONObjectID.generate.stringify))
  //  /** Generates a new date and adds it to your JSON using Json extended notation for BSON */
  //  val generateCreated = (__ \ 'info \ 'created \ '$date).json.put(JsNumber((new java.util.Date).getTime))
  //  /** Generates a new date and adds it to your JSON using Json extended notation for BSON */
  //  val generateUpdate = (__ \ 'info \ 'update \ '$date).json.put(JsNumber((new java.util.Date).getTime))
  //  /** Updates Json by adding both ID and date */
  //  val addMongoIdAndDate: Reads[JsObject] = __.json.update((generateID and generateCreated and generateUpdate).reduce)

  /**
   *
   */
  case class MyQueryBuilder(gqb: GenericQueryBuilder[JsObject, Reads, Writes]) {

    /**
     *
     */
    def sort(by: JsObject): MyQueryBuilder = {
      MyQueryBuilder(gqb.sort(by))
    }

    /**
     * Skip first s element
     * @param s number of element to skip
     * Implicit T -> JsObject must be in scope
     */
    def skip(s: Int): MyQueryBuilder = {
      MyQueryBuilder(gqb.options(QueryOpts(s)))
    }

    /**
     * Take only l element
     * @param l number of element to take
     * Implicit T -> JsObject must be in scope
     */
    def limit(l: Int): MyQueryBuilder = {
      val opt = gqb.options
      MyQueryBuilder(gqb.options(opt.batchSize(l)))
      //collection.find(Json.obj()).sort(sortBy).options(QueryOpts(skip, limit)).cursor[T].collect[List](limit)
    }

    /**
     * Return list of all result elements
     * Implicit T -> JsObject must be in scope
     */
    def toList: Future[List[T]] = {
      if (gqb.options.batchSizeN > 0)
        gqb.cursor[T].collect[List](gqb.options.batchSizeN)
      else
        gqb.cursor[T].collect[List](10000)
    }

    /**
     * Return an enumerator of all elements
     * Implicit T -> JsObject must be in scope
     */
    def toEnum: Enumerator[T] = {
      gqb.cursor[T].enumerate()
    }

    /**
     * skip parameter
     * Implicit T -> JsObject must be in scope
     */
    def one: Future[Option[T]] = {
      gqb.one[T]
    }

  }

  /**
   * Return a unique identificator
   */
  def generateId = BSONObjectID.generate.stringify

  /**
   * Return number of all documents on collection
   */
  def count: Future[Int] = collection.db.command(Count(collection.name))

  /**
   * Insert an element to collection,
   * Implicit T -> JsObject must be in scope
   */
  def insert(e: T, upsert: Boolean = false): Future[LastError] = {
    // val a = Json.toJson(e).transform(addMongoIdAndDate).map { jsobj =>
    if (upsert) collection.update(e, e, GetLastError(), upsert)
    else collection.insert(e)
    //    }
    //    a.get
  }

  /**
   * Update an element to collection with parameter e, the element to update is selected by parameter query
   * Implicit T -> JsObject must be in scope
   */
  def update(query: JsObject, e: JsObject, upsert: Boolean = false, multi: Boolean = false): Future[LastError] = {
    collection.update(query, e, GetLastError(), upsert, multi)
  }

  /**
   * Update an element to collection with parameter e, the element to update is selected by parameter query
   * Implicit T -> JsObject must be in scope
   */
  def update(query: JsObject, e: T, upsert: Boolean): Future[LastError] = {
    Json.toJson(e).transform((__ \ '_id).json.prune) match {
      case JsSuccess(s, p) => collection.update(query, Json.obj("$set" -> s), GetLastError(), upsert)
      case err: JsError => Json.toJson(e); collection.update(query, Json.obj("$set" -> ""), GetLastError(), upsert);
    }
  }

  /**
   * Remove an element from collection,
   * Implicit T -> JsObject must be in scope
   */
  def remove(e: T, firstMatchOnly: Boolean = false): Future[LastError] = {
    collection.remove(e, GetLastError(), firstMatchOnly)
  }

  /**
   * Return all element in collection
   * Implicit T -> JsObject must be in scope
   */
  def all: MyQueryBuilder =
    MyQueryBuilder(collection.find(Json.obj()))

  /**
   * Find elements in collection, sorted by sortBy parameter
   * Implicit T -> JsObject must be in scope
   */
  def find(selector: JsValue): MyQueryBuilder = {
    MyQueryBuilder(collection.find(selector))
  }

}