package storage.mongo.formatters

import play.api.libs.json._
import play.api.libs.functional.syntax._

import com.merle.play.storage.Entity
import com.merle.play.storage.mongo.StorageId

/**
 * Represent unique ID for storage DB
 */
object StorageIdFormats {

  implicit val storageFormat: Format[StorageId] = {
    /** because of single value of read, i have to do map, it's a bug of play's json library, but don't worry ;)*/
    val reads: Reads[StorageId] = (__ \ '$oid).read[String].map(s => StorageId.apply(s))

    import play.api.libs.json.Writes._
    /** because of single value of write, i have to do contramap (inverse map), it's a bug of play's json library, but don't worry ;)*/
    val writes: Writes[StorageId] = (__ \ '$oid).write[String].contramap(s => s.iod)

    Format(reads, writes)
  }

}
