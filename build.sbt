import bintray.AttrMap
import bintray._

name := """auth-module"""

description := "A common play module for authentication, it use play2-reactivemongo and play-silhouette"

organization := "com.merle"

version := "0.0.1"

scalaVersion := "2.11.1"

resolvers += "Sonatype Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/"

libraryDependencies ++= Seq(
  "com.mohiva" %% "play-silhouette" % "2.0-SNAPSHOT",
  "com.typesafe.play.plugins" %% "play-plugins-mailer" % "2.3.0",
  "org.reactivemongo" %% "play2-reactivemongo" % "0.10.5.akka23-SNAPSHOT"
)       

lazy val root = (project in file(".")).enablePlugins(PlayScala)  
 
publishMavenStyle := true
 
bintrayPublishSettings
 
bintray.Keys.repository in bintray.Keys.bintray := "maven"
 
licenses += ("MIT", url("http://opensource.org/licenses/MIT"))
 
bintray.Keys.bintrayOrganization in bintray.Keys.bintray := None